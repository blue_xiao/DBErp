<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Admin\Controller;

use Admin\Entity\PrintTemplate;
use Admin\Form\PrintTemplateForm;
use Admin\Service\PrintTemplateManager;
use Doctrine\ORM\EntityManager;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\I18n\Translator;

class PrintTemplateController extends AbstractActionController
{
    private $translator;
    private $entityManager;
    private $printTemplateManager;

    private $printTemplateArray = [];

    public function __construct(
        Translator      $translator,
        EntityManager   $entityManager,
        PrintTemplateManager $printTemplateManager
    )
    {
        $this->translator       = $translator;
        $this->entityManager    = $entityManager;
        $this->printTemplateManager = $printTemplateManager;

        $this->printTemplateArray = [
            'otherInStock'          => $this->translator->translate('其他入库单'),
            'otherOutStock'         => $this->translator->translate('其他出库单'),
            'stockCheck'            => $this->translator->translate('库存盘点单'),
            'stockTransfer'         => $this->translator->translate('库间调拨单'),

            'purchaseOrder'         => $this->translator->translate('采购单'),
            'purchaseOrderReturn'   => $this->translator->translate('采购退货单'),
            'purchaseWarehouseOrder'=> $this->translator->translate('采购入库单'),

            'salesOrder'            => $this->translator->translate('销售订单'),
            'salesSendOrder'        => $this->translator->translate('销售发货单'),
            'salesOrderReturn'      => $this->translator->translate('销售退货单'),

            'receivables'           => $this->translator->translate('应收账款单'),
            'payable'               => $this->translator->translate('应付账款单'),

            'shopOrder'             => $this->translator->translate('商城订单'),
        ];
    }

    /**
     * 打印模板列表
     * @return array[]
     */
    public function indexAction(): array
    {
        $printTemplateList = $this->entityManager->getRepository(PrintTemplate::class)->findAll();

        return ['printTemplateList' => $printTemplateList];
    }

    /**
     * 编辑打印模板
     * @return array|\Laminas\Http\Response
     * @throws \Doctrine\ORM\Exception\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editAction()
    {
        $templateKey = $this->params()->fromRoute('id');

        $templateInfo = $this->entityManager->getRepository(PrintTemplate::class)->findOneBy(['templateCode' => $templateKey]);
        if ($templateInfo == null) {
            $this->flashMessenger()->addWarningMessage($this->translator->translate('该打印模板不存在！'));
            return $this->redirect()->toRoute('print-template');
        }

        $form = new PrintTemplateForm();

        if($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);
            if ($form->isValid()) {
                $data = $form->getData();
                $this->printTemplateManager->editPrintTemplate($data, $templateInfo);

                $this->adminCommon()->addOperLog($this->translator->translate('打印模板设置成功！'), $this->translator->translate('管理员'));
                return $this->redirect()->toRoute('print-template');
            }
        } else $form->setData($templateInfo->valuesArray());

        //echo $templateInfo->getTemplateBody();
        return [
            'templateInfo' => $templateInfo,
            'form'  => $form
        ];
    }
}